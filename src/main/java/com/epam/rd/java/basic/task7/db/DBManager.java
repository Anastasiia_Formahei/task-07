package com.epam.rd.java.basic.task7.db;


import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;



public class DBManager {
	private static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
	private static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
	private static final String INSERT_USER_IN_TEAM = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";
	private static final String FIND_USERS = "SELECT * FROM users";
	private static final String FIND_TEAMS = "SELECT * FROM teams";
	private static final String FIND_USER = "SELECT * FROM users WHERE login =?";
	private static final String FIND_TEAM = "SELECT * FROM teams WHERE name =?";
	private static final String DELETE_USER = "DELETE FROM users WHERE login = ?";
	private static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
	private static final String UPDATE_TEAM = "UPDATE teams SET name =? WHERE id =?";
	private static final String FIND_TEAMS_BY_USER_ID = "SELECT t.id, t.name FROM users_teams ut\n"
			+ "JOIN users u ON ut.user_id = u.id\n" + "JOIN teams t ON ut.team_id = t.id\n" + "WHERE u.id = ?";

	private static DBManager instance;
    public static synchronized DBManager getInstance() {
		if(instance == null) instance = new DBManager();
		return instance;
	}

	public DBManager(){}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Connection con = connection();
				PreparedStatement ps = con.prepareStatement(FIND_USERS)){
			try (ResultSet resultSet = ps.executeQuery()){
				while (resultSet.next()){
					User user = new User();
					user.setId(resultSet.getInt("id"));
					user.setLogin(resultSet.getString("login"));
					users.add(user);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new  DBException("Exception ==>", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {

		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try (Connection con = connection()){
			ps = con.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, user.getLogin());

			if (ps.executeUpdate() != 1) {
				return false;
			}
			resultSet = ps.getGeneratedKeys();
			if (resultSet.next()) {
				int idField = resultSet.getInt(1);
				user.setId(idField);
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new  DBException("Exception ==>", e);
		}finally {
			try {
				resultSet.close();
				ps.close();
			}catch (SQLException ex){
				ex.printStackTrace();
			}
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
			try (Connection con = connection();
				 PreparedStatement ps = con.prepareStatement(DELETE_USER)){
				for(User user: users){
					ps.setString(1, user.getLogin());
					ps.executeUpdate();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new  DBException("Exception ==>", e);
			}

		return false;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		try (Connection con = connection();
			 PreparedStatement ps = con.prepareStatement(FIND_USER)){
			ps.setString(1, login);
			try (ResultSet resultSet = ps.executeQuery()){
				while (resultSet.next()){

					user.setId(resultSet.getInt("id"));
					user.setLogin(resultSet.getString("login"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new  DBException("Exception ==>", e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try (Connection con = connection();
			 PreparedStatement ps = con.prepareStatement(FIND_TEAM)){
			ps.setString(1, name);
			try (ResultSet resultSet = ps.executeQuery()){
				while (resultSet.next()) {
					team.setId(resultSet.getInt("id"));
					team.setName(resultSet.getString("name"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new  DBException("Exception ==>", e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection con = connection();
			 PreparedStatement ps = con.prepareStatement(FIND_TEAMS)){
			try(ResultSet rs = ps.executeQuery()){
				while (rs.next()){
					Team team = new Team();
					team.setId(rs.getInt("id"));
					team.setName(rs.getString("name"));
					teams.add(team);
				}
			}
		}catch (SQLException e) {
			e.printStackTrace();
			throw new  DBException("Exception ==>", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try (Connection con = connection()){
			ps = con.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, team.getName());

			if (ps.executeUpdate() != 1) {
				return false;
			}
			resultSet = ps.getGeneratedKeys();
			if (resultSet.next()) {
				int idField = resultSet.getInt(1);
				team.setId(idField);
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new  DBException("Exception ==>", e);
		}finally {
			try {
				resultSet.close();
				ps.close();
			}catch (SQLException ex){
				ex.printStackTrace();
			}
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException{
		Connection con = null;
		PreparedStatement ps;
		con = connection();
		try{
            ps = con.prepareStatement(INSERT_USER_IN_TEAM);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			for(Team team: teams){
                ps.setInt(1, getUser(user.getLogin()).getId());
                ps.setInt(2, getTeam(team.getName()).getId());
                ps.addBatch();
                int [] users = ps.executeBatch();
                for(int i :users ){
                    if (i !=1) return false;
                }
			}
			con.commit();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new  DBException("Exception ==>", e);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {Connection con = connection();
			 ps = con.prepareStatement(FIND_TEAMS_BY_USER_ID);
		ps.setInt(1, user.getId());
		rs = ps.executeQuery();
		while (rs.next()) {
			Team team = new Team();
			teams.add(team);
			team.setId(rs.getInt(1));
			team.setName(rs.getString(2));
		}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new  DBException("Exception ==>", e);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection con = connection();
			 PreparedStatement ps = con.prepareStatement(DELETE_TEAM)){
			ps.setInt(1, team.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new  DBException("Exception ==>", e);
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException{
		try (Connection con = connection();
			 PreparedStatement ps = con.prepareStatement(UPDATE_TEAM)){
				ps.setString(1, team.getName());
				ps.setInt(2, team.getId());
				ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new  DBException("Exception ==>", e);
		}
		return false;
	}

	private static Connection connection(){
		try (FileReader reader = new FileReader("app.properties");){
			Properties properties = new Properties();
			properties.load(reader);
			return DriverManager.getConnection(properties.getProperty("connection.url"));

		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
